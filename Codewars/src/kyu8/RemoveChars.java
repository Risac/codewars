package kyu8;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/**
 * Your goal is to create a function that removes the first and last characters
 * of a string. You're given one parameter, the original string. You don't have
 * to worry with strings with less than two characters.
 */
public class RemoveChars {

	@Test
	public void testRemoveChars() {

		String str = "Parameter";

		assertEquals(removeChars(str), "aramete");

	}

	// Method solution
	static String removeChars(String str) {
		return str.substring(1, str.length() - 1);
	}
}
