package kyu8;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/**
 * Your task is to create a function that does four basic mathematical
 * operations.
 * 
 * The function should take three arguments - operation(string/char),
 * value1(number), value2(number). The function should return result of numbers
 * after applying the chosen operation.
 * 
 * Examples(Operator, value1, value2) --> output ('+', 4, 7) --> 11 ('-', 15,
 * 18) --> -3 ('*', 5, 5) --> 25 ('/', 49, 7) --> 7
 */
public class BasicMathOperations {

	@Test
	public void testBasicMath() {

		int v1 = 6;
		int v2 = 3;

		String op = "+";
		assertEquals(basicMath(op, v1, v2), 9);

		op = "-";
		assertEquals(basicMath(op, v1, v2), 3);

		op = "*";
		assertEquals(basicMath(op, v1, v2), 18);

		op = "/";
		assertEquals(basicMath(op, v1, v2), 2);

	}

//Method solution
	static Integer basicMath(String op, int v1, int v2) {
		int result = 0;

		if (op.equals("+")) {
			result = v1 + v2;
		} else if (op.equals("-")) {
			result = v1 - v2;
		} else if (op.equals("*")) {
			result = v1 * v2;
		} else if (op.equals("/") && v2 != 0) {
			result = v1 / v2;
		} else if (v2 == 0) {
			System.out.println("Division by zero!");
		}
		return result;
	}

}
