package kyu8;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/**
 * Complete the method that takes a boolean value and return a "Yes" string for
 * true, or a "No" string for false.
 */
public class ConvertBool {

	@Test
	public void testBoolToWord() {
		boolean a = true;
		boolean b = false;

		assertEquals(boolToWord(a), "Yes");
		assertEquals(boolToWord(b), "No");

	}

	public static void main(String[] args) {
		boolean b = true;

		// 2 quick solutions
		System.out.println(b ? ("Yes") : ("No"));

		System.out.println(boolToWord(b));
	}

	// Method solution
	static String boolToWord(boolean b) {
		String s = "";

		if (b == true) {
			s = "Yes";
		} else {
			s = "No";
		}
		return s;
	}

}
