package kyu8;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;

import org.junit.Test;

/**
 * Given an array of integers.
 * 
 * Return an array, where the first element is the count of positives numbers
 * and the second element is sum of negative numbers.
 * 
 * If the input array is empty or null, return an empty array.
 * 
 * Example For input [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15],
 * you should return [10, -65].
 */
public class CountPositivesSumNegatives {

	@Test
	public void testCountPositivesSumNegatives() {

		int[] input = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15 };
		int[] result = { 10, -65 };

		assertArrayEquals(countPositivesSumNegatives(input), result);

	}

//Method solution 1
	static int[] countPositivesSumNegatives(int[] input) {
		int count = 0;
		int sum = 0;
		int[] result = new int[2];

		if (input == null || input.length == 0) {
			return new int[] {};
		}

		for (int number : input) {
			if (number > 0) {
				count++;
				result[0] = count;
			} else {
				sum += number;
				result[1] = sum;
			}
		}
		return result;
	}

	// Method solution 2
	static int[] countPositivesSumNegatives2(int[] input) {

		if (input == null || input.length == 0) {
			return new int[] {};
		} else {
			return new int[] { (int) Arrays.stream(input).filter(i -> i > 0).count(),
					(int) Arrays.stream(input).filter(i -> i < 0).sum() };
		}

	}
}
