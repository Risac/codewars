package kyu8;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

/**
 * Given a string of digits, you should replace any digit below 5 with '0' and
 * any digit 5 and above with '1'. Return the resulting string.
 */
public class FakeBinary {

	@Test
	public void testFakeBin() {

		assertEquals(fakeBin("326718234118"), "001101000001");
		assertEquals(fakeBin2("326718234118"), "001101000001");
	}

//Method solution 1
	static String fakeBin(String numberString) {
		String result = "";

		for (int i = 0; i < numberString.length(); i++) {
			char ch = numberString.charAt(i);

			if (ch < '5') {
				ch = '0';
			} else {
				ch = '1';
			}
			result += ch;
		}
		return result;
	}

	// Method solution 2
	static String fakeBin2(String numberString) {

		return numberString.chars().map(Character::getNumericValue).mapToObj(ch -> {
			if (ch < 5)
				return "0";
			else
				return "1";
		}).collect(Collectors.joining());
	}
}
