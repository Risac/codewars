package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/**
 * Return the number (count) of vowels in the given string.
 * 
 * We will consider a, e, i, o, u as vowels for this Kata (but not y).
 * 
 * The input string will only consist of lower case letters and/or spaces.
 */

public class VowelCount {

	@Test
	public void testGetCount() {

		String test = "pipeline";

		assertEquals(getCount(test), 4);
		assertEquals(getCount2(test), 4);
		assertEquals(getCount3(test), 4);
	}

	// Method solution 1
	static int getCount(String str) {
		int vowelsCount = 0;

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
				vowelsCount++;
			}
		}
		return vowelsCount;
	}

	// Method solution 2
	static int getCount2(String str) {
		return str.replaceAll("[^aeiou]", "").length();
	}

	// Method solution 3
	static int getCount3(String str) {

		return (int) str.chars().mapToObj(i -> (char) i).filter(i -> "aeiou".contains(String.valueOf(i))).count();
	}

}
