package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * You are going to be given a word. Your job is to return the middle character
 * of the word. If the word's length is odd, return the middle character. If the
 * word's length is even, return the middle 2 characters.
 * 
 * #Examples:
 * 
 * Kata.getMiddle("test") should return "es"
 * 
 * Kata.getMiddle("testing") should return "t"
 * 
 * Kata.getMiddle("middle") should return "dd"
 * 
 * Kata.getMiddle("A") should return "A" #Input
 * 
 * A word (string) of length 0 < str < 1000 (In javascript you may get slightly
 * more than 1000 in some test cases due to an error in the test cases). You do
 * not need to test for this. This is only here to tell you that you do not need
 * to worry about your solution timing out.
 * 
 * #Output
 * 
 * The middle character(s) of the word represented as a string.
 */
public class GetMiddleCharacter {

	@Test
	public void testGetMiddle() {

		assertEquals("es", getMiddle("test"));
		assertEquals("dd", getMiddle("middle"));
		assertEquals("t", getMiddle("testing"));
		assertEquals("A", getMiddle("A"));

	}

	// Method solution
	static String getMiddle(String word) {
		String newWord = "";
		char ch = word.charAt(word.length() / 2);

		if (word.length() % 2 != 0) {
			newWord += ch;
			return newWord;
		} else {
			char ch2 = word.charAt((word.length() / 2) - 1);
			newWord += ch2;
			newWord += ch;
			return newWord;
		}
	}

}
