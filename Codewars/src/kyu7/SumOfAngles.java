package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/**
 * Find the total sum of internal angles (in degrees) in an n-sided simple
 * polygon. N will be greater than 2.
 */
public class SumOfAngles {

	@Test
	public void testSumOfAngles() {

		assertEquals(180, sumOfAngles(3));
		assertEquals(360, sumOfAngles(4));
	}

	static int sumOfAngles(int n) {
		int sum = 0;
		for (int i = 2; i < n; i++) {
			sum += 180;
		}
		return sum;
	}

}
