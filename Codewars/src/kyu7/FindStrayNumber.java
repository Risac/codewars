package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * You are given an odd-length array of integers, in which all of them are the
 * same, except for one single number.
 * 
 * Complete the method which accepts such an array, and returns that single
 * different number.
 * 
 * The input array will always be valid! (odd-length >= 3)
 * 
 * Examples [1, 1, 2] ==> 2 [17, 17, 3, 17, 17, 17, 17] ==> 3
 */
public class FindStrayNumber {

	@Test
	public void testStray() {
		assertEquals(2, getActualFor(1, 1, 2));
	}

	private int getActualFor(int... numbers) {
		return stray(numbers);
	}

	private int stray(int[] numbers) {
		int different = numbers[0];
		int count = 0;

		for (int number : numbers) {
			if (different != number) {
				different = number;
				count++;
				if (numbers[count] == different) {
					return numbers[0];
				}
				return different;
			}
		}
		return 0;
	}

}
