package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

/**
 * Given two numbers and an arithmetic operator (the name of it, as a string),
 * return the result of the two numbers having that operator used on them.
 * 
 * a and b will both be positive integers, and a will always be the first number
 * in the operation, and b always the second.
 * 
 * The four operators are "add", "subtract", "divide", "multiply".
 * 
 * A few examples:(Input1, Input2, Input3 --> Output)
 * 
 * 5, 2, "add" --> 7 5, 2, "subtract" --> 3 5, 2, "multiply" --> 10 5, 2,
 * "divide" --> 2.5 Try to do it without using if statements!
 */
public class ArithmeticFunction {

	@Test
	public void testArithmetic() {

		assertEquals(3, arithmetic(1, 2, "add"));
		assertEquals(6, arithmetic(8, 2, "subtract"));
		assertEquals(10, arithmetic(5, 2, "multiply"));
		assertEquals(4, arithmetic(8, 2, "divide"));
		assertEquals(3, arithmetic(1, 2, "add"));
		assertEquals(6, arithmetic(8, 2, "subtract"));
		assertEquals(10, arithmetic(5, 2, "multiply"));
		assertEquals(4, arithmetic(8, 2, "divide"));
		assertNotEquals(5, arithmetic(8, 2, "divide"));

		// Sercond solution tests
		assertEquals(3, arithmetic2(1, 2, "add"));
		assertEquals(6, arithmetic2(8, 2, "subtract"));
		assertEquals(10, arithmetic2(5, 2, "multiply"));
		assertEquals(4, arithmetic2(8, 2, "divide"));
		assertEquals(3, arithmetic2(1, 2, "add"));
		assertEquals(6, arithmetic2(8, 2, "subtract"));
		assertEquals(10, arithmetic2(5, 2, "multiply"));
		assertEquals(4, arithmetic2(8, 2, "divide"));
		assertNotEquals(5, arithmetic2(8, 2, "divide"));

	}

	// Method solution 1
	public static int arithmetic(int a, int b, String operator) {
		if (operator.equals("add")) {
			return a + b;
		}
		if (operator.equals("subtract")) {
			return a - b;
		}
		if (operator.equals("multiply")) {
			return a * b;
		}
		if (b != 0) {
			if (operator.equals("divide")) {
				return a / b;
			}
		} else {
			System.out.println("Division by zero!");
		}
		return 0;
	}

	// Method solution 2 without if statements
	public static int arithmetic2(int a, int b, String operator) {
		switch (operator) {
		case "add":
			return a + b;
		case "subtract":
			return a - b;
		case "multiply":
			return a * b;
		case "divide":
			switch (b) {
			case 0:
				System.out.println("Division by zero!");
				return 0;
			default:
				return a / b;
			}
		}
		return 0;
	}
}
