package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Trolls are attacking your comment section!
 * 
 * A common way to deal with this situation is to remove all of the vowels from
 * the trolls' comments, neutralizing the threat.
 * 
 * Your task is to write a function that takes a string and return a new string
 * with all vowels removed.
 * 
 * For example, the string "This website is for losers LOL!" would become "Ths
 * wbst s fr lsrs LL!".
 * 
 * Note: for this kata y isn't considered a vowel.
 */
public class DisemvowelTrolls {

	@Test
	public void testDisemvowel() {

		assertEquals("Ths wbst s fr lsrs LL!", disemvowel("This website is for losers LOL!"));
		assertEquals("N ffns bt,\nYr wrtng s mng th wrst 'v vr rd",
				disemvowel("No offense but,\nYour writing is among the worst I've ever read"));
		assertEquals("Wht r y,  cmmnst?", disemvowel("What are you, a communist?"));

		// Second solution tests
		assertEquals("Ths wbst s fr lsrs LL!", disemvowel2("This website is for losers LOL!"));
		assertEquals("N ffns bt,\nYr wrtng s mng th wrst 'v vr rd",
				disemvowel2("No offense but,\nYour writing is among the worst I've ever read"));
		assertEquals("Wht r y,  cmmnst?", disemvowel2("What are you, a communist?"));

	}

	// Method solution 1
	static String disemvowel(String str) {
		String newStr = "";

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);

			if (ch != 'a' && ch != 'e' && ch != 'i' && ch != 'o' && ch != 'u' && ch != 'A' && ch != 'E' && ch != 'I'
					&& ch != 'O' && ch != 'U') {
				newStr += ch;
			}
		}
		return newStr;
	}

	// Method solution 2
	static String disemvowel2(String str) {
		return str.replaceAll("[aeiouAEIOU]", "");

	}

}
