package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Complete the function that accepts a string parameter, and reverses each word
 * in the string. All spaces in the string should be retained.
 * 
 * Examples "This is an example!" ==> "sihT si na !elpmaxe" "double spaces" ==>
 * "elbuod secaps"
 */
public class ReverseWords {

	@Test
	public void testReverseWords() {

		assertEquals("ehT kciuq nworb xof spmuj revo eht yzal .god",
				reverseWords("The quick brown fox jumps over the lazy dog."));
		assertEquals("elppa", reverseWords("apple"));
		assertEquals("a b c d", reverseWords("a b c d"));
		assertEquals("elbuod  decaps  sdrow", reverseWords("double  spaced  words"));

	}

	static String reverseWords(final String original) {
		String wordResult = "";
		String sentenceReversed = "";

		for (int i = 0; i < original.length(); i++) {
			char ch = original.charAt(i);
			wordResult = ch + wordResult;

			if (ch == ' ') {
				sentenceReversed += wordResult.trim() + " ";
				wordResult = "";
			}
		}
		sentenceReversed = sentenceReversed + wordResult;
		return sentenceReversed;
	}

}
