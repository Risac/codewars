package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Simple, given a string of words, return the length of the shortest word(s).
 * 
 * String will never be empty and you do not need to account for different data
 * types.
 */
public class ShortestWord {

	@Test
	public void testFindShort() {

		assertEquals(3, findShort("bitcoin take over the world maybe who knows perhaps"));
		assertEquals(3, findShort2("bitcoin take over the world maybe who knows perhaps"));
		assertEquals(3, findShort3("bitcoin take over the world maybe who knows perhaps"));

		assertEquals(3, findShort("turns out random test cases are easier than writing out basic ones"));
		assertEquals(3, findShort2("turns out random test cases are easier than writing out basic ones"));
		assertEquals(3, findShort3("turns out random test cases are easier than writing out basic ones"));

		assertEquals(2, findShort("Let's travel abroad shall we"));
		assertEquals(2, findShort2("Let's travel abroad shall we"));
		assertEquals(2, findShort3("Let's travel abroad shall we"));

	}

	// Method solution 1
	static int findShort(String s) {
		int count = 0;

		String[] array = s.split(" ", 0);
		int[] newarray = new int[array.length];

		for (int i = 0; i < array.length; i++) {
			String s2 = array[i];
			count = s2.length();
			newarray[i] = count;
		}
		Arrays.sort(newarray);
		return newarray[0];
	}

	// Method solution 2
	static int findShort2(String s) {

		List<Integer> length = new ArrayList<>();
		String[] split = s.split(" ", 0);
		for (String string : split) {
			int x = string.length();
			length.add(x);
		}
		Collections.sort(length);
		return length.get(0);
	}

	// Method solution 3
	static int findShort3(String s) {
		return Arrays.stream(s.split(" ")).mapToInt(String::length).min().getAsInt();
	}
}
