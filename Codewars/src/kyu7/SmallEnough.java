package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

/**
 * You will be given an array and a limit value. You must check that all values
 * in the array are below or equal to the limit value. If they are, return true.
 * Else, return false.
 * 
 * You can assume all values in the array are numbers.
 */
public class SmallEnough {

	@Test
	public void testSmallENough() {

		assertEquals(true, smallEnough(new int[] { 66, 101 }, 200));
		assertEquals(false, smallEnough(new int[] { 78, 117, 110, 99, 104, 117, 107, 115 }, 100));
		assertEquals(true, smallEnough(new int[] { 101, 45, 75, 105, 99, 107 }, 107));
		assertEquals(true, smallEnough(new int[] { 80, 117, 115, 104, 45, 85, 112, 115 }, 120));

	}

	// Method solution 1
	public static boolean smallEnough(int[] a, int limit) {

		boolean test = false;

		for (int x : a) {
			if (x <= limit) {
				test = true;
			} else {
				return false;
			}
		}
		return test;
	}

	// Method solution 2
	public static boolean smallEnough2(int[] a, int limit) {
		return Arrays.stream(a).noneMatch(i -> i > limit);
	}
}
