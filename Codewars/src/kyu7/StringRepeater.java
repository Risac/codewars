package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/**
 * Write a function named repeater() that takes two arguments (a string and a
 * number), and returns a new string where the input string is repeated that
 * many times.
 * 
 * Example: (Input1, Input2 --> Output) "a", 5 --> "aaaaa"
 */
public class StringRepeater {

	@Test
	public void testRepeat() {
		assertEquals("aaaaa", repeat("a", 5));
		assertEquals("NaNaNaNaNaNaNaNaNaNaNaNaNaNaNaNa", repeat("Na", 16));
		assertEquals("Wub Wub Wub Wub Wub Wub ", repeat("Wub ", 6));
	}

	public static String repeat(String string, long n) {

		String repeat = "";

		for (int i = 0; i < n; i++) {
			repeat += string;

		}
		return repeat;
	}
}
