package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Check to see if a string has the same amount of 'x's and 'o's. The method
 * must return a boolean and be case insensitive. The string can contain any
 * char.
 * 
 * Examples input/output:
 * 
 * XO("ooxx") => true XO("xooxx") => false XO("ooxXm") => true XO("zpzpzpp") =>
 * true // when no 'x' and 'o' is present should return true XO("zzoo") => false
 */
public class OAndE {

	@Test
	public void testGotXO() {
		assertEquals(true, getXO("xxxooo"));
		assertEquals(true, getXO("xxxXooOo"));
		assertEquals(false, getXO("xxx23424esdsfvxXXOOooo"));
		assertEquals(false, getXO("xXxxoewrcoOoo"));
		assertEquals(false, getXO("XxxxooO"));
		assertEquals(true, getXO("zssddd"));
		assertEquals(false, getXO("Xxxxertr34"));
	}

	// Method solution 1
	static boolean getXO(String str) {
		int countX = 0;
		int countO = 0;

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (ch == 'x' || ch == 'X') {
				countX++;
			}
			if (ch == 'o' || ch == 'O') {
				countO++;
			}
		}
		if (countX == countO) {
			return true;
		}
		return false;
	}

	// Method solution 2
	static boolean getXO2(String str) {

		long countX = str.toLowerCase().chars().filter(c -> c == 'x').count();
		long countO = str.toLowerCase().chars().filter(c -> c == 'o').count();

		return countX == countO;
	}

}
