package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * In mathematics, the factorial of a non-negative integer n, denoted by n!, is
 * the product of all positive integers less than or equal to n. For example: 5!
 * = 5 * 4 * 3 * 2 * 1 = 120. By convention the value of 0! is 1.
 * 
 * Write a function to calculate factorial for a given input. If input is below
 * 0 or above 12 throw an exception of type ArgumentOutOfRangeException (C#) or
 * IllegalArgumentException (Java) or RangeException (PHP) or throw a RangeError
 * (JavaScript) or ValueError (Python) or return -1 (C).
 */
public class Factorial {

	@Test
	public void testFactorial() {

		assertEquals(1, factorial(0));
		assertEquals(6, factorial(3));
		assertEquals(120, factorial(5));

	}

	static int factorial(int n) {

		int factorial = 1;
		int value = n;

		if (n < 0 || n > 12) {
			throw new IllegalArgumentException();
		}
		for (int i = 0; i < n; i++) {
			factorial *= value;
			value--;
		}
		return factorial;
	}
}
