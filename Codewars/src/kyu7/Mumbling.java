package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

/**
 * This time no story, no theory. The examples below show you how to write
 * function accum:
 * 
 * Examples: accum("abcd") -> "A-Bb-Ccc-Dddd" accum("RqaEzty") ->
 * "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy" accum("cwAt") -> "C-Ww-Aaa-Tttt" The
 * parameter of accum is a string which includes only letters from a..z and
 * A..Z.
 */
public class Mumbling {

	@Test
	public void testAccum() {
		assertEquals(accum("ZpglnRxqenU"),
				"Z-Pp-Ggg-Llll-Nnnnn-Rrrrrr-Xxxxxxx-Qqqqqqqq-Eeeeeeeee-Nnnnnnnnnn-Uuuuuuuuuuu");
		assertEquals(accum("NyffsGeyylB"),
				"N-Yy-Fff-Ffff-Sssss-Gggggg-Eeeeeee-Yyyyyyyy-Yyyyyyyyy-Llllllllll-Bbbbbbbbbbb");
		assertEquals(accum("MjtkuBovqrU"),
				"M-Jj-Ttt-Kkkk-Uuuuu-Bbbbbb-Ooooooo-Vvvvvvvv-Qqqqqqqqq-Rrrrrrrrrr-Uuuuuuuuuuu");
		assertEquals(accum("EvidjUnokmM"),
				"E-Vv-Iii-Dddd-Jjjjj-Uuuuuu-Nnnnnnn-Oooooooo-Kkkkkkkkk-Mmmmmmmmmm-Mmmmmmmmmmm");
		assertEquals(accum("HbideVbxncC"),
				"H-Bb-Iii-Dddd-Eeeee-Vvvvvv-Bbbbbbb-Xxxxxxxx-Nnnnnnnnn-Cccccccccc-Ccccccccccc");

		assertNotEquals(accum("ZpglnRxqenG"),
				"Z-Pp-Ggg-Llll-Nnnnn-Rrrrrr-Xxxxxxx-Qqqqqqqq-Eeeeeeeee-Nnnnnnnnnn-Uuuuuuuuuuu");
	}

	static String accum(String s) {
		String newString = "";

		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			newString += Character.toUpperCase(ch);

			for (int x = 0; x < i; x++) {
				ch = s.charAt(i);
				newString += Character.toLowerCase(ch);
			}
			if (i != s.length() - 1)
				newString += "-";
		}
		return newString;
	}
}
