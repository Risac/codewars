package kyu7;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

/**
 * Given an integer as input, can you round it to the next (meaning, "higher")
 * multiple of 5?
 * 
 * Examples:
 * 
 * input: output: 0 -> 0 2 -> 5 3 -> 5 12 -> 15 21 -> 25 30 -> 30 -2 -> 0 -5 ->
 * -5 etc. Input may be any positive or negative integer (including 0).
 * 
 * You can assume that all inputs are valid integers.
 */
public class RoundUpTo5 {

	@Test
	public void testRoundToNext5() {

		int[][] arr = { { 0, 0 }, { 1, 5 }, { 3, 5 }, { 5, 5 }, { 7, 10 }, { 39, 40 } };
		Arrays.stream(arr).forEach((testCase) -> {
			assertEquals("Input: " + testCase[0], testCase[1], roundToNext5(testCase[0]));
		});
	}

	static int roundToNext5(int number) {
		int result = 0;

		if (number == 0) {
			return 0;
		}
		for (int i = 0; i < number + 1; i++) {
			if (number < result) {
				return result;
			}
			if (number == result) {
				return number;
			}
			result = 5 * i;
		}
		if (number < 0) {
			for (int i = 0; i > number; i++) {
				result = (-1) * (5 * i);
				if (number > result) {
					return result + 5;
				}
			}
		}
		return result;
	}

}
