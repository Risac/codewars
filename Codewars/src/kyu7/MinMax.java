package kyu7;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

/**
 * Ben has a very simple idea to make some profit: he buys something and sells
 * it again. Of course, this wouldn't give him any profit at all if he was
 * simply to buy and sell it at the same price. Instead, he's going to buy it
 * for the lowest possible price and sell it at the highest.
 * 
 * Task Write a function that returns both the minimum and maximum number of the
 * given list/array.
 * 
 * Examples MinMax.minMax(new int[]{1,2,3,4,5}) == {1,5} MinMax.minMax(new
 * int[]{2334454,5}) == {5, 2334454} MinMax.minMax(new int[]{1}) == {1, 1}
 * Remarks All arrays or lists will always have at least one element, so you
 * don't need to check the length. Also, your function will always get an array
 * or a list, you don't have to check for null, undefined or similar.
 */
public class MinMax {

	@Test
	public void testMinMax() {

		assertArrayEquals(new int[] { 1, 5 }, minMax(new int[] { 1, 2, 3, 4, 5 }));
		assertArrayEquals(new int[] { 1, 5 }, minMax2(new int[] { 1, 2, 3, 4, 5 }));

		assertArrayEquals(new int[] { 5, 2334454 }, minMax(new int[] { 2334454, 5 }));
		assertArrayEquals(new int[] { 5, 2334454 }, minMax2(new int[] { 2334454, 5 }));

		assertArrayEquals(new int[] { 1, 1 }, minMax(new int[] { 1 }));
		assertArrayEquals(new int[] { 1, 1 }, minMax2(new int[] { 1 }));

	}

	// Method solution 1
	static int[] minMax(int[] arr) {
		int[] minmax = new int[2];
		Arrays.sort(arr);

		minmax[0] = arr[0];
		minmax[1] = arr[arr.length - 1];

		return minmax;
	}

	// Method solution 2
	static int[] minMax2(int[] arr) {
		int min = Arrays.stream(arr).min().getAsInt();
		int max = Arrays.stream(arr).max().getAsInt();

		return new int[] { min, max };

	}

}
