package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.junit.Test;

/**
 * Your team is writing a fancy new text editor and you've been tasked with
 * implementing the line numbering.
 * 
 * Write a function which takes a list of strings and returns each line
 * prepended by the correct number.
 * 
 * The numbering starts at 1. The format is n: string. Notice the colon and
 * space in between.
 * 
 * Examples:
 * 
 * number(Arrays.asList()) # => [] number(Arrays.asList("a", "b", "c")) // =>
 * ["1: a", "2: b", "3: c"]
 */
public class Testing123 {

	@Test
	public void testNumber() {

		assertEquals(Arrays.asList("1: a", "2: b", "3: c"), number(Arrays.asList("a", "b", "c")));
		assertEquals(Arrays.asList("1: ", "2: ", "3: ", "4: ", "5: "), number(Arrays.asList("", "", "", "", "")));

	}

	// Method solution 1
	static List<String> number(List<String> lines) {
		List<String> newLines = new ArrayList<String>();

		int counter = 1;

		for (String a : lines) {
			String str = counter + ": " + a;
			newLines.add(str);
			counter++;

		}

		return newLines;
	}

	// Method solution 2
	static List<String> number2(List<String> lines) {

		AtomicInteger counter = new AtomicInteger(1);

		return lines.stream().map(s -> String.valueOf(counter.getAndIncrement()) + ": " + s)
				.collect(Collectors.toList());
	}
}
