package kyu7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

/**
 * Write a function partlist that gives all the ways to divide a list (an array)
 * of at least two elements into two non-empty parts.
 * 
 * Each two non empty parts will be in a pair (or an array for languages without
 * tuples or a structin C - C: see Examples test Cases - ) Each part will be in
 * a string Elements of a pair must be in the same order as in the original
 * array. Examples of returns in different languages: a = ["az", "toto",
 * "picaro", "zone", "kiwi"] --> [["az", "toto picaro zone kiwi"], ["az toto",
 * "picaro zone kiwi"], ["az toto picaro", "zone kiwi"], ["az toto picaro zone",
 * "kiwi"]] or a = {"az", "toto", "picaro", "zone", "kiwi"} --> {{"az", "toto
 * picaro zone kiwi"}, {"az toto", "picaro zone kiwi"}, {"az toto picaro", "zone
 * kiwi"}, {"az toto picaro zone", "kiwi"}} or a = ["az", "toto", "picaro",
 * "zone", "kiwi"] --> [("az", "toto picaro zone kiwi"), ("az toto", "picaro
 * zone kiwi"), ("az toto picaro", "zone kiwi"), ("az toto picaro zone",
 * "kiwi")] or a = [|"az", "toto", "picaro", "zone", "kiwi"|] --> [("az", "toto
 * picaro zone kiwi"), ("az toto", "picaro zone kiwi"), ("az toto picaro", "zone
 * kiwi"), ("az toto picaro zone", "kiwi")] or a = ["az", "toto", "picaro",
 * "zone", "kiwi"] --> "(az, toto picaro zone kiwi)(az toto, picaro zone
 * kiwi)(az toto picaro, zone kiwi)(az toto picaro zone, kiwi)"
 */
public class PartsOfAList {

	@Test
	public void testPartList() {

		String[] s1 = new String[] { "cdIw", "tzIy", "xDu", "rThG" };
		String a = "[[cdIw, tzIy xDu rThG], [cdIw tzIy, xDu rThG], [cdIw tzIy xDu, rThG]]";
		assertEquals(Arrays.deepToString(partlist(s1)), a);

		s1 = new String[] { "I", "wish", "I", "hadn't", "come" };
		a = "[[I, wish I hadn't come], [I wish, I hadn't come], [I wish I, hadn't come], [I wish I hadn't, come]]";
		assertEquals(Arrays.deepToString(partlist(s1)), a);

		s1 = new String[] { "vJQ", "anj", "mQDq", "sOZ" };
		a = "[[vJQ, anj mQDq sOZ], [vJQ anj, mQDq sOZ], [vJQ anj mQDq, sOZ]]";
		assertEquals(Arrays.deepToString(partlist(s1)), a);
	}

	public static String[][] partlist(String[] arr) {

		String[][] s = new String[arr.length - 1][2];
		String s0 = "";
		String s1 = "";

		// Create first part of array
		for (int i = 0; i < arr.length - 1; i++) {
			s0 += arr[i];

			// Add spaces if it isn't the last word
			if (i < arr.length - 1) {
				s0 += " ";
			}

			// Create second part of array
			for (int k = i + 1; k < arr.length; k++) {
				s1 += arr[k];

				if (k < arr.length - 1) {
					s1 += " ";
				}
			}
			// Add to array
			s[i][0] = s0.trim();
			s[i][1] = s1.trim();

			s1 = ""; // Reset second part of array
		}
		return s;
	}
}
