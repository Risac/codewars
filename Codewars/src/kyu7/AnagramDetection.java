package kyu7;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

/**
 * An anagram is the result of rearranging the letters of a word to produce a
 * new word (see wikipedia).
 * 
 * Note: anagrams are case insensitive
 * 
 * Complete the function to return true if the two arguments given are anagrams
 * of each other; return false otherwise.
 * 
 * Examples "foefet" is an anagram of "toffee"
 * 
 * "Buckethead" is an anagram of "DeathCubeK"
 */
public class AnagramDetection {

	@Test
	public void testAnagram() {

		String one = "foefet";
		String two = "toffee";

		boolean is = isAnagram(one, two);
		assertTrue(is);
	}

	// Method solution 1
	static boolean isAnagram(String test, String original) {

		List<Character> orig = new ArrayList<Character>();
		List<Character> tst = new ArrayList<Character>();

		if (original.length() == test.length()) {
			for (int i = 0; i < original.length(); i++) {
				char ch = original.toLowerCase().charAt(i);
				char ch2 = test.toLowerCase().charAt(i);
				orig.add(ch);
				tst.add(ch2);
			}
			orig.sort(null);
			tst.sort(null);

			if (orig.equals(tst))
				return true;
		}
		return false;
	}

	// Method solution 2
	static boolean isAnagram2(String test, String original) {
		int[] t1 = test.toLowerCase().chars().sorted().toArray();
		int[] t2 = original.toLowerCase().chars().sorted().toArray();

		if (Arrays.compare(t1, t2) == 0) {
			return true;
		} else {
			return false;
		}
	}

	// Method solution 3
	static boolean isAnagram3(String test, String original) {
		return Stream.of(test.toLowerCase().split("")).sorted().collect(Collectors.joining())
				.equals(Stream.of(original.toLowerCase().split("")).sorted().collect(Collectors.joining()));
	}
}
