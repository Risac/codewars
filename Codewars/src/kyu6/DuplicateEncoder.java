package kyu6;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/**
 * The goal of this exercise is to convert a string to a new string where each
 * character in the new string is "(" if that character appears only once in the
 * original string, or ")" if that character appears more than once in the
 * original string. Ignore capitalization when determining if a character is a
 * duplicate.
 * 
 * Examples "din" => "(((" "recede" => "()()()" "Success" => ")())())" "(( @" =>
 * "))(("
 */
public class DuplicateEncoder {

	@Test
	public void testMethod() {

		String x = "din";
		String result = encode2(x);
		String exp = "(((";

		assertEquals(exp, result);

	}

	// Method solution 1
	static String encode(String word) {

		String word2 = word.toLowerCase();
		String newWord = "";
		char newChar = 0;

		// If String consists of only 0 or 1 characters
		if (word2.length() == 0) {
			return newWord = "";
		}
		if (word2.length() == 1) {
			return newWord = "(";
		}

		for (int i = 0; i < word2.length(); i++) {
			char ch = word2.charAt(i);

			for (int k = word2.indexOf(ch) + 1; k < word2.length(); k++) {
				char ch2 = word2.charAt(k);

				if (ch == ch2) {
					newChar = ')';
					break;
				} else {
					newChar = '(';
				}
			}
			newWord += newChar;

			// Reset char to '(' for the last char in String
			newChar = '(';
		}
		return newWord;
	}

	// Method solution 2
	static String encode2(String word) {

		char[] charArray = word.toLowerCase().toCharArray();
		StringBuilder sb = new StringBuilder();

		for (char character : charArray) {
			if (word.toLowerCase().chars().filter(ch -> ch == character).count() > 1)
				sb.append(")");
			else
				sb.append("(");
		}
		return sb.toString();
	}
}
