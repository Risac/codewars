package kyu6;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * There is an array with some numbers. All numbers are equal except for one.
 * Try to find it!
 * 
 * Kata.findUniq(new double[]{ 1, 1, 1, 2, 1, 1 }); // => 2 Kata.findUniq(new
 * double[]{ 0, 0, 0.55, 0, 0 }); // => 0.55 It�s guaranteed that array contains
 * at least 3 numbers.
 * 
 * The tests contain some very huge arrays, so think about performance.
 */
public class FindUniqueNum {

	@Test
	public void testFindUniq() {

		double[] tested = { 1, 1, 1, 1, 1, 3, 1, 1 };
		double expected = 3;

		double findUniq = findUniq(tested);

		assertEquals(expected, findUniq, 0);
	}

	static double findUniq(double arr[]) {

		double current = 0;
		double first = arr[0];
		double last = arr[arr.length - 1];

		for (int i = 0; i < arr.length; i++) {
			current = arr[i];

			if (first != current) {
				if (first == last) {
					return current;
				} else {
					if (current == last && i > 1) {
						return current;
					} else {
						return first;
					}
				}
			}
		}
		return current;
	}
}
