package kyu6;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

/**
 * Born a misinterpretation of this kata, your task here is pretty simple: given
 * an array of values and an amount of beggars, you are supposed to return an
 * array with the sum of what each beggar brings home, assuming they all take
 * regular turns, from the first to the last.
 * 
 * For example: [1,2,3,4,5] for 2 beggars will return a result of [9,6], as the
 * first one takes [1,3,5], the second collects [2,4].
 * 
 * The same array with 3 beggars would have in turn have produced a better out
 * come for the second beggar: [5,7,3], as they will respectively take [1,4],
 * [2,5] and [3].
 * 
 * Also note that not all beggars have to take the same amount of "offers",
 * meaning that the length of the array is not necessarily a multiple of n;
 * length can be even shorter, in which case the last beggars will of course
 * take nothing (0).
 * 
 * Note: in case you don't get why this kata is about English beggars, then you
 * are not familiar on how religiously queues are taken in the kingdom ;)
 * 
 * Note 2: do not modify the input array.
 */
public class EnglishBeggars {

	@Test
	public void testBeggars() {
		int[] test = { 1, 2, 3, 4, 5 };
		int[] a1 = { 15 }, a2 = { 9, 6 }, a3 = { 5, 7, 3 }, a4 = { 1, 2, 3, 4, 5, 0 }, a5 = {};

		assertArrayEquals(a1, beggars(test, 1));
		assertArrayEquals(a2, beggars(test, 2));
		assertArrayEquals(a3, beggars(test, 3));
		assertArrayEquals(a4, beggars(test, 6));
		assertArrayEquals(a5, beggars(test, 0));
	}

	static int[] beggars(int[] values, int n) {

		int[] result = new int[n];
		int sum = 0;
		int loop = 0;

		for (int i = 0; i < n; i++) {

			for (int k = loop; k < values.length; k++) {
				sum += values[k];
				k = (k - 1) + n;
			}
			result[i] = sum;
			sum = 0;
			loop++;
		}
		return result;
	}
}
