package kyu6;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * You probably know the "like" system from Facebook and other pages. People can
 * "like" blog posts, pictures or other items. We want to create the text that
 * should be displayed next to such an item.
 * 
 * Implement the function which takes an array containing the names of people
 * that like an item. It must return the display text as shown in the examples:
 * 
 * [] --> "no one likes this" ["Peter"] --> "Peter likes this" ["Jacob", "Alex"]
 * --> "Jacob and Alex like this" ["Max", "John", "Mark"] --> "Max, John and
 * Mark like this" ["Alex", "Jacob", "Mark", "Max"] --> "Alex, Jacob and 2
 * others like this"
 */
public class WhoLikesIt {

	@Test
	public void testWhoLikesIt() {
		assertEquals("no one likes this", whoLikesIt());
		assertEquals("Peter likes this", whoLikesIt("Peter"));
		assertEquals("Jacob and Alex like this", whoLikesIt("Jacob", "Alex"));
		assertEquals("Max, John and Mark like this", whoLikesIt("Max", "John", "Mark"));
		assertEquals("Alex, Jacob and 2 others like this", whoLikesIt("Alex", "Jacob", "Mark", "Max"));
	}

	public static String whoLikesIt(String... names) {
		String result = null;

		if (names.length == 0) {
			return result = "no one likes this";
		}
		if (names.length == 1) {
			return result = names[0] + " likes this";
		}
		if (names.length == 2) {
			return result = names[0] + " and " + names[1] + " like this";
		}
		if (names.length == 3) {
			return result = names[0] + ", " + names[1] + " and " + names[2] + " like this";
		}
		if (names.length >= 4) {
			return result = names[0] + ", " + names[1] + " and " + (names.length - 2) + " others like this";
		}
		return result;
	}
}
