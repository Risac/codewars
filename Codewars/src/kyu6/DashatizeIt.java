package kyu6;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

/**
 * Given a variable n,
 * 
 * If n is an integer, Return a string with dash'-'marks before and after each
 * odd integer, but do not begin or end the string with a dash mark. If n is
 * negative, then the negative sign should be removed.
 * 
 * If n is not an integer, return an empty value.
 * 
 * Ex:
 * 
 * dashatize(274) -> '2-7-4' dashatize(6815) -> '68-1-5'
 */
public class DashatizeIt {

	@Test
	public void testDashatize() {

		assertEquals("2-7-4", dashatize(274));
		assertEquals("5-3-1-1", dashatize(5311));
		assertEquals("86-3-20", dashatize(86320));
		assertEquals("9-7-4-3-02", dashatize(974302));

		assertEquals("0", dashatize(0));
		assertEquals("1", dashatize(-1));
		assertEquals("28-3-6-9", dashatize(-28369));

		assertEquals("2-1-4-7-48-3-64-7", dashatize(Integer.MAX_VALUE));
		assertEquals("2-1-4-7-48-3-648", dashatize(Integer.MIN_VALUE));
		assertEquals("1-1-1-1-1-1-1-1-1-1", dashatize(-1111111111));

	}

	static String dashatize(int num) {
		Predicate<Integer> isOdd = x -> (x % 2 != 0);
		String result = "";
		String n = "";
		int prev = 0;
		int current = 0;
		int counter = 0;
		boolean exception = false;

		if (num == 0) {
			return result = "" + 0;
		}
		if (num < 0) {
			if (num == Integer.MIN_VALUE) {
				num = num + 1;
				exception = true;
			}
			num = -1 * num;
		}

		while (num > 0) {
			current = num % 10;

			if (exception == true) {
				current = (num % 10) + 1;
				n = "-" + current;
				exception = false;
			}
			if (isOdd.test(current)) {
				n = "-" + num % 10;
				if (isOdd.test(prev)) {
					n = "-" + num % 10;
				} else if (counter != 0) {
					n = "-" + num % 10 + "-";
				}
			} else {
				n = "" + current;
			}

			if (num < 10) {
				if (isOdd.test(current) && !isOdd.test(prev)) {
					n = "" + num % 10 + "-";

					if (result.length() < 2) {
						n = "" + num % 10;
					}
				} else {
					n = "" + num % 10;
				}
			}
			num = num / 10;
			prev = current;
			counter++;
			result = n + result;
		}
		return result;
	}
}
